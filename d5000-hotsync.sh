#!/bin/bash
CLASS=d5000
BASE=/photos/$CLASS
NOW=`date +%Y-%m-%dT%H:%M:%S`
SYNCDIR=$BASE/hotsync/$NOW
VIEWDIR=$BASE/hotview/$NOW
THUMBDIR=/thumbs/$CLASS/0/$NOW
umask 022
mkdir $SYNCDIR || exit 1
mkdir $VIEWDIR || exit 1
cd $SYNCDIR || exit 1
gphoto2 --list-files > contents.txt
grep -v "There is no file in folder" contents.txt \
| sed -n "s/There .* files in folder '\(.*\)'./\1/p" \
| head -1 > folder.txt
grep -v "There is no file in folder" contents.txt \
| sed -n "s/There .* \([0-9]* files\) in folder '\(.*\)'./\1/p" \
| head -1
gphoto2 --port usb: --camera "USB PTP Class Camera" --get-all-files --folder `cat folder.txt` || exit 1

for f in *.JPG
do
  d=$(jhead $f \
      |sed -n 's/Date.Time *: '//p\
      |sed -e s/:/-/ -e s/:/-/ )
  if [ -n "$d" ]
  then
      touch -d "$d" $f 
  fi
done
touch complete
chmod 444 *

/home/ceder/rgit/photofokus/imagemap.sh $CLASS $NOW

chown -R ceder:ceder $VIEWDIR $THUMBDIR
chmod 644 $SYNCDIR/*

echo file://$VIEWDIR/part-1.html
