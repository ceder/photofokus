(require 'widget)

(eval-when-compile
  (require 'wid-edit)
  (require 'dired))

;;; End-user configuration.

(defvar pf-root "/photos")

(defvar pf-thumbs-root "/thumbs")

(defvar pf-classes '("olymp" "dc" "other" "s520" "magic" "d5000" "nexus"
		     "moto-g7"
		     "moto-g32"
		     "kristina"
		     "kristina-moto-g5"
		     "kristina-moto-g30"
		     "kristina-moto-g32"
		     "kristina-moto-g34"))

(defvar pf-clear-cache-interval 50
  "*Clear the image cache once this many images has been displayed.")

(defvar pf-output-directory "~/saved-images"
  "*Where to save output files.")

(defvar pf-remote-mode nil
  "*Set to t to display small images.")

;;; Constants.

(defvar pf-sync-re
  "^[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]T[0-9][0-9]:[0-9][0-9]:[0-9][0-9]$"
  "Regexp that matches hotsync directories.")

(defconst pf-optimizers
  '((pf-AND pf-optimize-and-and pf-optimize-and-not)
    (pf-OR pf-optimize-or-or)))

;;; Variables.

(defvar pf-images nil
  "List of images.")

(defvar pf-clear-cache-count 0)

(defvar pf-private-mode nil
  "Non-nil means don't display images marked as private.")

(defvar pf-current-image nil
  "Sequence number of the currently displayed image.")

(defvar pf-tag-paste-bin nil
  "Paste buffer for tags.")

(defvar pf-uncompiled-filter nil
  "Active filter, before being compiled.")

(defvar pf-compiled-filter nil
  "Active filter, after compilation and optimization.")

(defvar pf-active-list nil
  "List of active images.")

(defvar pf-tag-index nil
  "Hash table from tag name to list of image numbers.")

(defvar pf-marked-image nil
  "*The currently marked image")

(defvar pf-tag-history nil)
(defvar pf-last-tag nil)

(defvar pf-private-widget)
(defvar pf-exclude-slideshow-widget)
(defvar pf-any-change)

;;; File handling.

(defun pf-base (class)
  (expand-file-name class pf-root))

(defun pf-thumbs (class)
  (expand-file-name class pf-thumbs-root))

;;; The pf-image datatype.

(defun pf-image (class synctime image-filename)
  "Create an image metadata object.
CLASS should be selected among the values of `pf-classes'.
SYNCTIME is the name of the hotsync directory.
IMAGE-FILENAME is the name of the image within the hotsync directory."
  ; 0 'pf-image-type
  ; 1 class
  ; 2 synctime
  ; 3 image filename
  ; 4 timestamp (or nil if file not statted yet)
  ; 5 assoc list of properties (0 means not loaded yet)
  ; 6 n (index on pf-images)
  ; 7 rotation (or nil if not loaded yet)
  (unless (memq class pf-classes)
    (error "Bad class %s specified" class))
  (vector 'pf-image-type class synctime image-filename nil 0 nil nil))

(defun pf-image->prefetch (obj)
  (pf-image->rotation obj)
  (pf-image->timestamp obj)
  (pf-image->load-properties obj))

(defun pf-image->class (obj)
  (aref obj 1))

(defun pf-image->base (obj)
  (pf-base (pf-image->class obj)))

(defun pf-image->metadata-root (obj)
  (expand-file-name
   (pf-image->class obj)
   (expand-file-name "metadata" pf-root)))

(defun pf-image->synctime (obj)
  (aref obj 2))

(defun pf-image->image-filename (obj)
  (aref obj 3))

(defun pf-image->properties (obj)
  (aref obj 5))

(defun pf-image->set-properties (obj alist)
  (aset obj 5 alist))

(defun pf-image->filename (obj)
  (unless (eq (aref obj 0) 'pf-image-type)
    (signal 'wrong-type-argument (list 'pf-image obj)))
  (expand-file-name
   (aref obj 3)
   (expand-file-name
    (aref obj 2)
    (expand-file-name
     "hotsync"
     (pf-image->base obj)))))

(defun pf-image->rotated-filename (obj)
  "Return the filename of a correctly rotated full-size image.
Return the original file name where possible."
  (let ((angle (pf-image->rotation obj)))
    (if (= angle 0)
	(pf-image->filename obj)
      (let ((rotated-filename (pf-image->thumbnail obj 'fullsize)))
	(unless (file-exists-p rotated-filename)
	  (make-directory (file-name-directory rotated-filename) t)
	  (call-process "/bin/sh" nil nil nil
			"-c" (concat
			      (format "jpegtran -copy all -rotate %s"
				      (- 360 angle))
			      " < "
			      (shell-quote-argument (pf-image->filename obj))
			      " > "
			      (shell-quote-argument rotated-filename))))
	rotated-filename))))


(defun pf-image->thumbnail (obj size)
  "Return the filename to a thumbnail of OBJ which is SIZE pixels large.
SIZE may also be the symbol 'fullsize, which refers to the full
unscaled image, but rotated to the proper orientation."
  (unless (eq (aref obj 0) 'pf-image-type)
    (signal 'wrong-type-argument (list 'pf-image obj)))
  (expand-file-name
   (aref obj 3)
   (expand-file-name
    (cond
     ((equal size 'fullscreen)
      (let ((res "fullsize"))
	(save-match-data
	  (if (file-readable-p "/tmp/natural-fullsize.txt")
	      (let ((size (pf-read-file "/tmp/natural-fullsize.txt")))
		(if (string-match "\\([0-9]+\\) \\([0-9]+\\)\n" size)
		    (setq res (format "%sx%s"
				      (match-string 1 size)
				      (match-string 2 size)))))))
	res))
     ((equal size 'fullsize)
      "fullsize")
     (t (format "%d" size)))
    (expand-file-name
     (aref obj 2)
     (expand-file-name
      (format "%d" (pf-image->rotation obj))
      (pf-thumbs (pf-image->class obj)))))))

(defun pf-image->timestamp (obj)
  "Return the file modification time stamp of OBJ as a list of two integers.
The first integer has the high-order 16 bits of time, the second
has the low 16 bits.  The time is read from the hotsync directory."
  (unless (eq (aref obj 0) 'pf-image-type)
    (signal 'wrong-type-argument (list 'pf-image obj)))
  (or
   (aref obj 4)
   (aset obj 4 (elt (file-attributes (pf-image->filename obj)) 5))))

(defun pf-image->forget-timestamp (obj)
  "Forget the timestamp, so that pf-image->timestamp will read it from the filesystem."
  (unless (eq (aref obj 0) 'pf-image-type)
    (signal 'wrong-type-argument (list 'pf-image obj)))
  (pf-image->invalidate-cache obj)
  (aset obj 4 nil))

(defun pf-image->metadata (obj data)
  "Return the filename for metadata about OBJ of type DATA."
  (unless (eq (aref obj 0) 'pf-image-type)
    (signal 'wrong-type-argument (list 'pf-image obj)))
  (expand-file-name
   data
   (expand-file-name
    (aref obj 3)
    (expand-file-name
     (aref obj 2)
     (pf-image->metadata-root obj)))))

(defun pf-image->rotation (obj)
  "Return the current rotation of OBJ as an integer.
The returned value will be 0, 90, 180 or 270."
  (or (aref obj 7)
      (aset obj 7
	    (let ((fn (pf-image->metadata obj "rot")))
	      (if (not (file-exists-p fn))
		  0
		(let ((val (pf-read-file fn)))
		  (cond
		   ((string= val "90\n") 90)
		   ((string= val "180\n") 180)
		   ((string= val "270\n") 270)
		   (t (error "Bad contents of %s" fn)))))))))

(defun pf-image->set-rotation (obj rot)
  "Set the rotation of image OBJ to ROT.
ROT should be an integer, 0, 90, 180 or 270, and is measured
counterclockwise."
  (if (not (member rot '(0 90 180 270)))
      (error "Bad rotation %s" rot))
  (pf-image->invalidate-cache obj)
  (let ((fn (pf-image->metadata obj "rot")))
    (if (= 0 rot)
	(condition-case nil
	    (delete-file fn)
	  (file-error nil))
      (make-directory (file-name-directory fn) t)
      (pf-write-file fn (format "%d\n" rot))))
  (aset obj 7 rot))

(defun pf-compare-image-timestamp (a b)
  "Compare two pf-image objects A and B according to when the photo was taken."
  (let ((a-stamp (pf-image->timestamp a))
	(b-stamp (pf-image->timestamp b)))
    (cond
     ((< (car a-stamp) (car b-stamp)) t)
     ((> (car a-stamp) (car b-stamp)) nil)

     ((< (cadr a-stamp) (cadr b-stamp)) t)
     ((> (cadr a-stamp) (cadr b-stamp)) nil)

     (t
      ;; If the timestamps are equal, compare the file names, ignoring
      ;; the base directory.
      (string-lessp (concat (pf-image->synctime a)
			    (pf-image->image-filename a))
		    (concat (pf-image->synctime b)
			    (pf-image->image-filename b)))))))

(defun pf-image->cache-file (obj)
  (expand-file-name "cached.el"
		    (expand-file-name
		     (pf-image->synctime obj)
		     (expand-file-name
		      "metadata"
		      (pf-thumbs (pf-image->class obj))))))

(defun pf-image->invalidate-cache (obj)
  (let ((cache (pf-image->cache-file obj)))
    (if (file-exists-p cache)
	(delete-file cache))))

(defun pf-image->load-properties (obj)
  "Load all properties for OBJ, unless they are already loaded."
  (if (equal 0 (pf-image->properties obj))
      (pf-image->set-properties
       obj (pf-read-property-file (pf-image->metadata obj "props")))))

(defun pf-image->save-properties (obj)
  "Save all properties for OBJ."
  (unless (equal 0 (pf-image->properties obj))
    (pf-image->invalidate-cache obj)
    (let ((fn (pf-image->metadata obj "props")))
      (make-directory (file-name-directory fn) t)
      (pf-write-property-file fn (pf-image->properties obj)))))

(defun pf-image->get-prop (obj name)
  "Return properties of OBJ stored under NAME, as a list of strings."
  (pf-image->load-properties obj)
  (cdr-safe (assoc name (pf-image->properties obj))))

(defun pf-image->set-prop (obj name val)
  "Set properties of OBJ for NAME to VAL (a list of strings)."
  (pf-image->load-properties obj)
  (let* ((props (pf-image->properties obj))
	 (a (assoc name props)))
    (if (null a)
	(pf-image->set-properties obj (cons (cons name val) props))
      (setcdr a val))))

(defun pf-image->tags (obj)
  "Return the tags of OBJ as a list of strings."
  (pf-image->get-prop obj "tags"))

(defun pf-image->add-tag (obj tag)
  "Add a tag to an image."
  (let ((tags (pf-image->tags obj)))
    (unless (member tag tags)
      (pf-image->set-tags obj (cons tag tags)))))

(defun pf-image->remove-tag (obj tag)
  "Remove a tag from an image."
  (let ((tags (pf-image->tags obj)))
    (if (member tag tags)
      (pf-image->set-tags obj (delete tag tags)))))

(defun pf-image->set-tags (obj tags)
  "Set the tags of OBJ to TAGS."
  (if pf-tag-index
      (dolist (tag (pf-image->tags obj))
	(let ((newval (delq obj (gethash tag pf-tag-index))))
	  (if (null newval)
	      (remhash tag pf-tag-index)
	    (puthash tag newval pf-tag-index)))))
  (pf-image->set-prop obj "tags" tags)
  (if pf-tag-index
      (dolist (tag tags)
	(puthash tag
		 (cons obj (gethash tag pf-tag-index))
		 pf-tag-index))))

(defun pf-image-compare-n (a b)
  (< (pf-image->n a)
     (pf-image->n b)))

(defun pf-image->n (obj)
  "Return the number of this image."
  (unless (eq (aref obj 0) 'pf-image-type)
    (signal 'wrong-type-argument (list 'pf-image obj)))
  (aref obj 6))

(defun pf-image->set-n (obj n)
  "Set the number of this image."
  (unless (eq (aref obj 0) 'pf-image-type)
    (signal 'wrong-type-argument (list 'pf-image obj)))
  (aset obj 6 n))


(defun pf-image->insert-description (obj)
  "Insert a description of the image in the current buffer."
  (insert (format-time-string "%Y-%m-%d %H:%M:%S"
			      (pf-image->timestamp obj)))
  (insert (format " %5d\n\t%s\n"
		  (pf-image->n obj)
		  (pf-image->filename obj)))
  (dolist (tag (pf-image->tags obj))
    (insert (format "\t\t%s\n" tag)))
  (insert "\n"))

(defun pf-display-exif (obj)
  "Display EXIF data for image."
  (interactive (list (elt pf-images pf-current-image)))
  (with-output-to-temp-buffer " *EXIF*"
    (call-process "jhead" nil standard-output nil (pf-image->filename obj))))

;;; Simplified file I/O.

(defun pf-read-file (file)
  "Return the contents of FILE as a string."
  (let ((buf (generate-new-buffer "tmp"))
	(coding-system-for-read 'utf-8))
    (save-excursion
      (set-buffer buf)
      (insert-file-contents file)
      (prog1 (buffer-string)
	(kill-buffer buf)))))

(defun pf-write-file (file contents)
  "Create FILE and write CONTENTS to it."
  (let ((buf (generate-new-buffer "tmp"))
	(require-final-newline nil))
    (save-excursion
      (set-buffer buf)
      (insert contents)
      (write-file file)
      (kill-buffer buf))))

(defun pf-read-property-file (file)
  (if (file-exists-p file)
      (let ((buf (generate-new-buffer "tmp")))
	(save-excursion
	  (set-buffer buf)
	  (let ((coding-system-for-read 'utf-8))
	    (insert-file-contents file))
	  (goto-char (point-min))
	  (prog1
	      (let ((res nil))
		(while (re-search-forward "^\\([^:]*\\): \\(.*\\)" nil t)
		  (let* ((key (match-string 1))
			 (val (match-string 2))
			 (a (assoc key res)))
		    (if (null a)
			(setq res (cons (list key val) res))
		      (setcdr a (nconc (cdr a) (list val)))))
		  (forward-line 1))
		res)
	    (kill-buffer buf))))
    nil))

(defun pf-write-property-file (file alist)
  (let ((buf (generate-new-buffer "tmp"))
	(require-final-newline nil))
    (save-excursion
      (set-buffer buf)
      (setq buffer-file-coding-system 'utf-8)
      (while alist
	(let ((key (car (car alist)))
	      (val (cdr (car alist))))
	  (while val
	    (if (string-match "\n" (car val))
		(error "val contains a newline"))
	    (if (string-match "\n" key)
		(error "key contains a newline"))
	    (insert (format "%s: %s\n" key (car val)))
	    (setq val (cdr val))))
	(setq alist (cdr alist)))
      (let (sort-fold-case)
	(sort-lines nil (point-min) (point-max)))
      (write-file file)
      (kill-buffer buf))))

;;; Top-level commands.

(defun pf-cache-up-to-date (class synctime git-commit cache-file commit-file)
  (and
   (file-readable-p commit-file)
   (file-readable-p cache-file)
   (let ((dir-commit (pf-read-file commit-file)))
     (or (string= git-commit dir-commit)
	 (let ((str (pf-run-git
		     (concat "diff --quiet "
			     dir-commit " " git-commit
			     " -- " class "/" synctime
			     " || echo differ"))))
	   (string= "" str))))))

(defun pf-load-one-syncdir (res git-commit class synctime thumbdir)
  (let* ((thumbsyncdir (expand-file-name synctime thumbdir))
	 (tinydir (expand-file-name "64" thumbsyncdir))
	 (imgdir (expand-file-name "512" thumbsyncdir))
	 (metadir (expand-file-name
		   synctime
		   (expand-file-name
		    "metadata"
		    (pf-thumbs class))))
	 (commit-file (expand-file-name "sha1" metadir))
	 (cache-file (expand-file-name "cached.el" metadir))
	 (images nil))
    (when (and (file-directory-p tinydir)
	       (file-directory-p imgdir))
      (cond
       ((pf-cache-up-to-date class synctime git-commit cache-file commit-file)
	(let ((buf (generate-new-buffer " *pf-read-buffer*"))
	      (coding-system-for-read 'utf-8))
	  (save-excursion
	    (set-buffer buf)
	    (insert-file-contents cache-file)
	    (setq images (read (current-buffer)))
	    (setcar res (+ (length images) (car res)))
	    (setcdr res (append images (cdr res)))
	    (kill-buffer buf)))
	(message "Found %d images" (car res)))
       (t
	(dolist (img (directory-files imgdir nil
				      "\\.[jJ][pP][gG]$" t))
	  (when (file-readable-p (expand-file-name img tinydir))
	    (let ((image (pf-image class synctime img)))
	      (setcdr res (cons image (cdr res)))
	      (setq images (cons image images)))
	    (setcar res (+ 1 (car res)))
	    (if (= (% (car res) 100) 0)
		(message "Found %d images" (car res)))))
	(dolist (img images)
	  (pf-image->prefetch img))
	(make-directory metadir t)
	(let ((coding-system-for-write 'utf-8))
	  (with-temp-file cache-file
	    (let ((print-level nil)
		  (print-length nil ))
	      (print images (current-buffer)))))))
      (let ((coding-system-for-write 'utf-8))
	(with-temp-file commit-file
	  (insert git-commit))))))


(defun pf-run-git (subcommand)
  (let* ((default-directory (file-name-as-directory
			     (expand-file-name "metadata"
					       pf-root)))
	 (res (shell-command-to-string (concat "git " subcommand))))
    (if (string= res "")
	res
      (substring res 0 -1))))

(defun pf-find-all-images ()
  "Create a list of all existing images, sorted on date.
For an image to be included, it must have thumbnails of sizes 64 and 512."
  (interactive)
  (setq pf-images
	;; res holds the number of images found so far in the car
	;; cell, and a list of all images in the cdr cell.
	(let ((res (cons 0 nil))
	      (git-commit (pf-run-git "rev-parse HEAD")))
	  (dolist (class pf-classes
			 (progn
			   (message "Found %d images. Sorting..." (car res))
			   (prog1
			       (sort (cdr res) 'pf-compare-image-timestamp)
			     (message "Found %d images. Sorting...done"
				      (car res)))))
	    (let ((thumbdir (expand-file-name
			     "0" (pf-thumbs class))))
	      (dolist (synctime
		       (directory-files
			thumbdir nil
			pf-sync-re
			t))
		(pf-load-one-syncdir res git-commit
				     class synctime thumbdir))))))
  (pf-update-n))

(defun pf-update-n ()
  (let ((n 0))
    (dolist (img pf-images)
      (pf-image->set-n img n)
      (setq n (1+ n))))
  (setq pf-tag-index nil))

(defun pf-find-all-tags ()
  (interactive)
  (let ((tags (make-hash-table :test 'equal :size 5000))
	(n 0))
    (dolist (image pf-images)
      (dolist (tag (pf-image->tags image))
	(puthash tag
		 (cons image (gethash tag tags))
		 tags))
      (let ((tag (concat "camera " (pf-image->class image))))
	(puthash tag
		 (cons image (gethash tag tags))
		 tags))
      (when (= 0 (% n 10000))
	(message "Indexing tags...%d" n))
      (setq n (1+ n)))
    (message "Indexing tags...%d done" n)
    (setq pf-tag-index tags)))

(defun pf-find-all ()
  (interactive)
  (pf-find-all-images)
  (pf-find-all-tags))

(defun pf-list-tags ()
  (interactive)
  (let ((buf (generate-new-buffer "*Photofokus-tags*")))
    (display-buffer buf)
    (set-buffer buf)
    (maphash (function (lambda (k v)
			 (insert (format "%-25s %5d\n" k (length v)))))
	     pf-tag-index)
    (let ((sort-fold-case nil))
      (sort-lines nil (point-min) (point-max)))))

(defun pf-copy-file-location-as-kill (obj)
  (interactive (list (elt pf-images pf-current-image)))
  (kill-new (pf-image->filename obj)))

(defvar photofokus-mode-map nil
  "Keymap used for the *Photofokus* buffer.")
(unless photofokus-mode-map
  (setq photofokus-mode-map (make-sparse-keymap))
  (set-keymap-parent photofokus-mode-map widget-keymap)
  (define-key photofokus-mode-map " " 'pf-add-tag)
  (define-key photofokus-mode-map (kbd "DEL") 'pf-remove-tag)
  (define-key photofokus-mode-map (kbd "C-SPC") 'pf-mark-image)
  (define-key photofokus-mode-map (kbd "M-SPC") 'pf-add-tag-range)
  (define-key photofokus-mode-map (kbd "M-<") 'pf-beginning)
  (define-key photofokus-mode-map (kbd "M->") 'pf-end)
  (define-key photofokus-mode-map "<" 'pf-beginning)
  (define-key photofokus-mode-map ">" 'pf-end)
  (define-key photofokus-mode-map "=" 'pf-diff-metadata-other-window)
  (define-key photofokus-mode-map "\C-x\C-x" 'pf-swap-mark)
  (define-key photofokus-mode-map "f" 'pf-goto-next)
  (define-key photofokus-mode-map "n" 'pf-goto-next)
  (define-key photofokus-mode-map "N" 'pf-goto-next-untagged)
  (define-key photofokus-mode-map "b" 'pf-goto-prev)
  (define-key photofokus-mode-map "e" 'pf-display-exif)
  (define-key photofokus-mode-map "p" 'pf-goto-prev)
  (define-key photofokus-mode-map "P" 'pf-goto-prev-untagged)
  (define-key photofokus-mode-map "d" 'pf-goto-date)
  (define-key photofokus-mode-map "s" 'pf-save-metadata)
  (define-key photofokus-mode-map "g" 'pf-goto-image)
  (define-key photofokus-mode-map "l" 'pf-rotate-image-ccw)
  (define-key photofokus-mode-map "r" 'pf-rotate-image-cw)
  (define-key photofokus-mode-map "o" 'pf-save-current-image)
  (define-key photofokus-mode-map "a" 'pf-toggle-excl-slideshow)
  (define-key photofokus-mode-map "t" 'pf-set-current-time)
  (define-key photofokus-mode-map "v" 'pf-view-current-image))

(define-derived-mode photofokus-mode fundamental-mode "PhotoFokus" nil)

(defun pf-maybe-clear-image-cache ()
  (setq pf-clear-cache-count (1+ pf-clear-cache-count))
  (when (>= pf-clear-cache-count pf-clear-cache-interval)
    (clear-image-cache t)
    (setq pf-clear-cache-count 0)))

(defface pf-highlighted-tag
  '((t :background "yellow"))
  "Face for highligting the last set tag.")

(defun pf-goto-image (n &optional even-if-unchanged)
  (interactive "nImage number: ")
  (pf-maybe-clear-image-cache)
  (if (>= n (length pf-images))
      (setq n (1- (length pf-images))))
  (let ((image (elt pf-images n))
	(modified nil))
    (let ((buf (get-buffer "*Photofokus*")))
      (when buf
	(set-buffer buf)
	(if (buffer-modified-p)
	    (if (not even-if-unchanged)
		(error "Unsaved changes exists")
	      (pf-remeber-metadata)
	      (setq modified t)))
	(kill-buffer buf)))
    (switch-to-buffer "*Photofokus*")
    (delete-other-windows)
    (kill-all-local-variables)
    (make-local-variable 'pf-private-widget)
    (make-local-variable 'pf-exclude-slideshow-widget)
    (let ((inhibit-read-only t))
      (erase-buffer))
    (photofokus-mode)
    (unless pf-remote-mode
      (dotimes (ix 4)
	(pf-insert-thumbnail (+ n ix -4) 64)))
    (widget-insert (format "%d" n))
    (unless pf-remote-mode
      (dotimes (ix 4)
	(pf-insert-thumbnail (+ n ix 1) 64)))
    (if (equal n pf-marked-image)
	(widget-insert "  MARK"))
    (widget-insert "\n")
    (pf-insert-thumbnail n (if pf-remote-mode 64 512))
    (widget-insert "\nTags:")
    (let ((separator " "))
      (dolist (tag (pf-image->tags image))
	(when (> (+ (current-column) (length tag) 2) (frame-width))
	  (widget-insert "\n")
	  (setq separator " "))
	(widget-insert separator (if (string= tag pf-last-tag)
				     (propertize tag 'face 'pf-highlighted-tag)
				   tag))
	(setq separator "\u00b7")))
    (widget-insert (format-time-string "\nPhoto taken: %Y-%m-%d %H:%M:%S\n"
				       (pf-image->timestamp image)))
    (widget-insert (pf-image->filename image))
    (widget-insert "\n")
    (setq pf-private-widget
	  (if pf-private-mode
	      nil
	    (widget-insert "Private: ")
	    (prog1
		(widget-create
		 'checkbox
		 :value (equal "1" (car-safe
				    (pf-image->get-prop image "private"))))
	      (widget-insert "\n"))))
    (widget-insert "Exclude from slideshow: ")
    (setq pf-exclude-slideshow-widget
	  (widget-create
	   'checkbox
	   :value (equal "1" (car-safe
			      (pf-image->get-prop image "excl-slideshow")))))
    (widget-insert "\n")
    (widget-create 'push-button :notify 'pf-rotate-image-ccw "Rot left")
    (widget-insert (format "%d" (pf-image->rotation image)))
    (widget-create 'push-button :notify 'pf-rotate-image-cw "Rot right")
    (widget-insert "\n")
    (widget-setup)
    (set-buffer-modified-p modified)
    (setq pf-current-image n)
    (goto-char (point-min))))

(defun pf-save-metadata ()
  (interactive)
  (if (not (buffer-modified-p))
      (message "(No changes need to be saved)")
    (pf-remeber-metadata)
    (pf-image->save-properties (elt pf-images pf-current-image))
    (set-buffer-modified-p nil)))

(defun pf-remeber-metadata ()
  (let ((image (elt pf-images pf-current-image)))
    (if pf-private-widget
	(pf-image->set-prop image "private"
			    (list (if (widget-value pf-private-widget)
				      "1"
				    "0"))))
    (if pf-exclude-slideshow-widget
	(pf-image->set-prop
	 image "excl-slideshow"
	 (list (if (widget-value pf-exclude-slideshow-widget)
		   "1"
		 "0"))))))

(defun pf-toggle-private ()
  "Toggle the \"private\" property for the current image."
  (interactive)
  (if pf-private-widget
      (widget-value-set pf-private-widget
			(not (widget-value pf-private-widget)))))

(defun pf-toggle-excl-slideshow ()
  "Toggle the \"excl-slideshow\" property for the current image."
  (interactive)
  (if pf-exclude-slideshow-widget
      (widget-value-set pf-exclude-slideshow-widget
			(not (widget-value pf-exclude-slideshow-widget)))))

(defun pf-read-tag (require-match)
  "Prompt the user for a tag.

This function should only be used in (interactive) declarations."
  (if (and require-match (null pf-tag-index))
      (pf-find-all-tags))
  (let ((prompt (if pf-last-tag
		    (format "Tag (default %s): " pf-last-tag)
		  "Tag: ")))
    (setq pf-last-tag
	  (if pf-tag-index
	      (completing-read prompt pf-tag-index nil require-match nil
			       'pf-tag-history pf-last-tag)
	    (read-string prompt nil 'pf-tag-history pf-last-tag)))))

(defun pf-set-last-tag (tag)
  (interactive (list (pf-read-tag t)))
  (setq pf-last-tag tag)
  (pf-goto-image pf-current-image t))

(defun pf-add-tag (tag)
  "Add a tag to the current image."
  (interactive (list (pf-read-tag nil)))
  (pf-image->add-tag (elt pf-images pf-current-image) tag)
  (set-buffer-modified-p t)
  (pf-goto-image pf-current-image t))

(defun pf-remove-tag (tag)
  "Remove a tag from the current image."
  (interactive
   (list
    (let* ((tags (pf-image->tags (elt pf-images pf-current-image)))
	   (default (if (member pf-last-tag tags)
			pf-last-tag
		      (car tags))))
      (if (null tags)
	  (error "No tag to remove"))
      (setq pf-last-tag
	    (completing-read (format "Tag (default %s): " default)
			     tags nil t nil
			     'pf-tag-history default)))))
  (let ((img (elt pf-images pf-current-image)))
    (if (not (member tag (pf-image->tags img)))
	(error "Tag not present"))
    (pf-image->remove-tag img tag))
  (set-buffer-modified-p t)
  (pf-goto-image pf-current-image t))

(defun pf-barf-if-unsaved ()
  (if (buffer-modified-p)
      (error "Unsaved changes exists")))

(defun pf-barf-if-no-mark ()
  (if (null pf-marked-image)
      (error "No image mark set")))

(defun pf-mark-image ()
  (interactive)
  (setq pf-marked-image pf-current-image)
  (pf-goto-image pf-current-image t)
  (message "Image mark set"))

(defun pf-beginning ()
  (interactive)
  (pf-barf-if-unsaved)
  (setq pf-marked-image pf-current-image)
  (pf-goto-image (if (null pf-active-list)
		     0
		   (pf-image->n (car pf-active-list)))))

(defun pf-end ()
  (interactive)
  (pf-barf-if-unsaved)
  (setq pf-marked-image pf-current-image)
  (pf-goto-image (if (null pf-active-list)
		     (1- (length pf-images))
		   (pf-image->n (car (last pf-active-list))))))

(defun pf-swap-mark ()
  (interactive)
  (pf-barf-if-no-mark)
  (pf-barf-if-unsaved)
  (let ((goal pf-marked-image))
    (setq pf-marked-image pf-current-image)
    (pf-goto-image goal)))

(defun pf-add-tag-range (tag)
  (interactive
   (progn
     (pf-barf-if-no-mark)
     (list (pf-read-tag nil))))
  (pf-barf-if-no-mark)
  (let* ((n (min pf-marked-image pf-current-image))
	 (end (max pf-marked-image pf-current-image)))
    (while (<= n end)
      (let ((img (elt pf-images n)))
	(pf-image->add-tag img tag)
	(if (/= n pf-current-image)
	    (pf-image->save-properties img)
	  (set-buffer-modified-p t)
	  (pf-goto-image n t)))
      (setq n (1+ n)))))

(defun pf-locate-on-active (goal)
  (let ((lo 0)
	(hi (length pf-active-list)))
    (while (< lo hi)
      (let* ((mid (/ (+ lo hi) 2))
	     (img-n (pf-image->n (elt pf-active-list mid))))
	(if (< img-n goal)
	    (setq lo (1+ mid))
	  (setq hi mid))))
    lo))

(defun pf-step (origin dir &optional limit)
  (if (null pf-active-list)
      (+ origin dir)
    (let* ((ix (pf-locate-on-active origin))
	   (n (cond
	       ((< ix (length pf-active-list))
		(if (and (> dir 0)
			 (/= origin (pf-image->n (elt pf-active-list ix))))
		    ix
		  (+ ix dir)))
	       ((> dir 0)
		(throw 'range nil))
	       (t
		(1- (length pf-active-list))))))

      (if (>= n (length pf-active-list))
	  (throw 'range nil))
      (if (< n 0)
	  (throw 'range nil))
      (pf-image->n (elt pf-active-list n)))))

(defun pf-step-safe (origin dir &optional limit)
  (catch 'range
    (pf-step origin dir limit)))

(defun pf-move (dir)
  (if (null (catch 'range
		(pf-goto-image (pf-step pf-current-image dir))))
      (error "No matching image.")))

(defun pf-image-excluded (n)
  (if (null pf-active-list)
      nil
    (let ((ix (pf-locate-on-active n)))
      (if (and (< ix (length pf-active-list))
	       (= n (pf-image->n (elt pf-active-list ix))))
	  nil
	t))))

(defun pf-goto-next ()
  (interactive)
  (pf-move 1))

(defun pf-goto-prev ()
  (interactive)
  (pf-move -1))

(defun pf-goto-random ()
  (interactive)
  (pf-goto-image (random (length pf-images))))

(defun pf-rotate-image-cw (&rest junk)
  "Rotate the current image 90� clockwise."
  (interactive)
  (pf-rotate-image 270))

(defun pf-rotate-image-ccw (&rest junk)
  "Rotate the current image 90� counterclockwise."
  (interactive)
  (pf-rotate-image 90))

(defun pf-rotate-image (amount)
  "Rotate the current image AMOUNT degrees counterclockwise."
  (let ((image (elt pf-images pf-current-image)))
    (pf-image->set-rotation image (% (+ amount (pf-image->rotation image))
				     360))
    (pf-create-thumbnail image 64)
    (pf-create-thumbnail image 512))
  (pf-goto-image pf-current-image t))

(defun pf-create-thumbnail (image size)
  (call-process "/usr/local/bin/updatethumbnails" nil nil nil
		"-i"
		"-s" (format "%d" size)
		"-c" (pf-image->class image)
		"-t" (pf-image->synctime image)))

(defun pf-insert-thumbnail (n size)
  (cond ((and (>= n 0) (< n (length pf-images)))
	 (let ((image (create-image
		       (pf-image->thumbnail (elt pf-images n) size)
		       nil nil
		       :margin (cons (/ size 16) 8)))
	       (start (point)))
	   (widget-create 'push-button
			  :notify 'pf-goto-image-widget
			  :pf-image n
			  " ")
	   (add-text-properties start (point)
				(list 'display image
				      'intangible image
				      'rear-nonsticky (list 'display)))))))

(defun pf-goto-image-widget (widget &rest unused)
  (let ((pos (if (eq (widget-at) widget) (point))))
    (pf-goto-image (widget-get widget :pf-image))
    (if pos
	(goto-char pos))))

(defun pf-inline-slideshow ()
  (interactive)
  (dotimes (x (length pf-images))
    (pf-goto-image x)
    (if (= (% x 50) 0)
	(clear-image-cache))
    (sit-for 0)))

(defun pf-save-current-image ()
  "Save the current image in `pf-output-directory'.
The image will be rotated properly."
  (interactive)
  (pf-save-image pf-current-image))

(defun pf-save-image (n &optional filename)
  "Save image N in `pf-output-directory'.
The image will be rotated properly.

Optional FILENAME is a complete file name where the file be saved.  If
supplied, `pf-output-directory' will be ignored."
  (let* ((image (elt pf-images n))
	 (angle (pf-image->rotation image))
	 (resdir (expand-file-name
		  (pf-image->synctime image)
		  pf-output-directory))
	 (resfile (or filename
		      (expand-file-name (pf-image->image-filename image)
					resdir))))
    (make-directory resdir t)
    (cond
     ((eq angle 0)
      (call-process "/bin/cp" nil nil nil
		    "--preserve=timestamps"
		    (pf-image->filename image)
		    resfile))
     (t
      (call-process "/bin/sh" nil nil nil
		    "-c" (concat
			  (format "jpegtran -copy all -rotate %s"
				  (- 360 angle))
			  " < "
			  (shell-quote-argument (pf-image->filename image))
			  " > "
			  (shell-quote-argument resfile)))
      (call-process "/usr/bin/touch" nil nil nil
		    "-r"
		    (pf-image->filename image)
		    resfile)))
    (message "Saved %s" resfile)))

(defun pf-insert-doctype-html ()
  "Insert a DOCTYPE and the <html> start tag."
  (insert "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\"\n")
  (insert "  \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n")
  (insert "<html xmlns=\"http://www.w3.org/1999/xhtml\"")
  (insert " lang=\"en\" xml:lang=\"en\">\n"))

(defun pf-export-contact-sheets (directory)
  "Create contact sheets for all images."
  (interactive "FWhere? ")
  (make-directory directory)

  (let ((indexbuf (find-file-noselect
		   (expand-file-name "index.html" directory)))
	(n 0)
	(chunk 0)
	chunkname)
    (set-buffer indexbuf)
    (erase-buffer)
    (pf-insert-doctype-html)
    (insert "<head><title>Image overview</title></head>\n")
    (insert "<body><h1>Image overview</h1>\n")
    (message "Creating contact sheet...")
    (while (< n (length pf-images))
      (setq chunk (1+ chunk))
      (setq chunkname (format "part-%03d.html" chunk))
      (set-buffer indexbuf)
      (insert
       (format
	"<a href=\"%s\">%s"
	chunkname
	(format-time-string "%Y-%m-%d %H:%M:%S" (pf-image->timestamp
						 (elt pf-images n)))))
      (let ((chunkbuf (find-file-noselect
		       (expand-file-name chunkname)))
	    (x 0))
	(message "Creating contact sheet %d..." n)
	(set-buffer chunkbuf)
	(erase-buffer)
	(pf-insert-doctype-html)
	(insert (format "<head><title>Chunk %d</title></head>\n" chunk))
	(insert (format
		 "<body><a href=\"part-%03d.html\">&lt;- Chunk %d -</a>"
		 (1- chunk) (1- chunk)))
	(insert (format " Chunk %d " chunk))
	(insert (format
		 "<a href=\"part-%03d.html\">- Chunk %d -&gt;</a>"
		 (1+ chunk) (1+ chunk)))
	(insert (format-time-string "%Y-%m-%d %H:%M:%S" (pf-image->timestamp
							 (elt pf-images n))))
	(insert "<br />")
	(while (and (< n (length pf-images)) (< x 210))
	  (let* ((img (elt pf-images n))
		 (excluded (equal "1" (car-safe
				       (pf-image->get-prop
					img
					"excl-slideshow")))))
	    (when (not (equal "1" (car-safe
				   (pf-image->get-prop
				    img
				    "private"))))
	      (message "Chunk %s" chunkbuf)
	      (set-buffer chunkbuf)
	      (if (= (% n 10) 0)
		  (insert (format " %d: " n)))
	      (if excluded
		  (insert "("))
	      (insert (format "<a href=\"%s\"><img src=\"%s\" alt=\"\" /></a> "
			      (pf-image->rotated-filename img)
			      (pf-image->thumbnail img 64)))
	      (if excluded
		  (insert ")"))
	      (when (= (% x 25) 0)
		(set-buffer indexbuf)
		(insert (format " <img src=\"%s\" alt=\"\" />"
				(pf-image->thumbnail img 64)))
		(set-buffer chunkbuf))
	      (setq x (1+ x)))
	    (setq n (1+ n))))
	(insert "\n</body></html>\n")
	(save-buffer)
	(kill-buffer chunkbuf)
	(set-buffer indexbuf)
	(insert (format " %d </a><br />\n" chunk)))
      (setq n (1+ n)))
    (insert "\n</body></html>\n")
    (save-buffer)
    (kill-buffer indexbuf)
    (message "Creating contact sheet... Done.")))

(defun pf-export-ceder-contacts ()
  (interactive)
  (require 'dired)
  (dired-delete-file "/home/ceder/contact" 'always)
  (pf-export-contact-sheets "/home/ceder/contact"))

(defun pf-goto-last-rotated ()
  (interactive)
  (let ((n (1- (length pf-images))))
    (while (and (> n 0)
		(= (pf-image->rotation (elt pf-images n)) 0))
      (setq n (1- n)))
    (pf-goto-image n)))

(defun pf-goto-next-untagged ()
  (interactive)
  (let ((n (1+ pf-current-image)))
    (while (and (< n (length pf-images))
		(not (null (pf-image->tags (elt pf-images n)))))
      (setq n (1+ n)))
    (pf-goto-image n)))

(defun pf-goto-prev-untagged ()
  (interactive)
  (let ((n (1- pf-current-image)))
    (while (and (> n -1)
		(not (null (pf-image->tags (elt pf-images n)))))
      (setq n (1- n)))
    (if (< n 0)
	(error "No previous untagged image"))
    (pf-goto-image n)))

(defun pf-tag-stats ()
  (interactive)
  (let ((tagged 0)
	(untagged 0))
    (dolist (img pf-images)
      (if (pf-image->tags img)
	  (setq tagged (1+ tagged))
	(setq untagged (1+ untagged))))
    (message "%d (%.2f%%) tagged, %d (%.2f%%) untagged"
	     tagged
	     (/ (* 100.0 tagged) (+ tagged untagged))
	     untagged
	     (/ (* 100.0 untagged) (+ tagged untagged)))))

(defun pf-slideshow (first last &optional force)
  (interactive (list (min pf-marked-image pf-current-image)
		     (1+ (max pf-marked-image pf-current-image))))
  (save-excursion
    (set-buffer (generate-new-buffer "*Slideshow*"))
    (let ((n first))
      (if (pf-image-excluded n)
	  (setq n (pf-step-safe n 1 last)))
      (while (and (not (null n)) (< n (length pf-images)) (< n last))
	(let* ((img (elt pf-images n))
	       (fullscreen-file (pf-image->thumbnail img 'fullscreen)))
	  (when (or force
		    (and (not (equal "1" (car-safe
					  (pf-image->get-prop
					   img
					   "private"))))
			 (not (equal "1" (car-safe
					  (pf-image->get-prop
					   img
					   "excl-slideshow"))))))
	    (insert (concat (if (file-readable-p fullscreen-file)
				fullscreen-file
			      (pf-image->rotated-filename img))
			    "\n"))))
	(setq n (pf-step-safe n 1 last))))
    ;; "--crt" before "--stdin" to use CRT-sized output.
    (if (= (point-min) (point-max))
	(error "No matching image"))
    (call-process-region (point-min) (point-max)
			 "/home/ceder/rgit/slidegtk/slidegtk.py"
			 t t nil "--fullscreen" "--stdin")))

(defun pf-save-slideshow (first last)
  (interactive "nFirst: \nnLast: ")
  (save-excursion
    (let ((n first)
	  (saved 0))
      (if (pf-image-excluded n)
	  (setq n (pf-step-safe n 1 last)))
      (while (and (not (null n)) (< n (length pf-images)) (< n last))
	(when (and (not (equal "1" (car-safe
				    (pf-image->get-prop
				     (elt pf-images n)
				     "private"))))
		   (not (equal "1" (car-safe
				    (pf-image->get-prop
				     (elt pf-images n)
				     "excl-slideshow")))))
	  (pf-save-image n)
	  (setq saved (1+ saved)))
	(setq n (pf-step-safe n 1 last)))
      (message "Saved %d images" saved))))

(defun pf-save-framkalla (directory)
  (interactive "GNon-existing directory to save in: ")
  (pf-filter-clear)
  (pf-filter-tag "framkalla")
  (pf-save-all-matching-images 1 (length pf-images) directory))

(defun pf-save-all-matching-images (first last directory)
  (interactive (list (min pf-marked-image pf-current-image)
		     (1+ (max pf-marked-image pf-current-image))
		     (expand-file-name
		      (read-file-name
		       "Non-existing directory to save in: "))))
  (make-directory directory)
  (save-excursion
    (let ((n first)
	  (saved 0))
      (if (pf-image-excluded n)
	  (setq n (pf-step-safe n 1 last)))
      (while (and (not (null n)) (< n last))
	(pf-save-image n
		       (expand-file-name (format "%05d.jpg" (1+ saved))
					 directory))
	(setq saved (1+ saved))
	(setq n (pf-step-safe n 1 last)))
      (message "Saved %d images" saved))))

(defun pf-view-current-image ()
  "View the current image as a slideshow."
  (interactive)
  (pf-slideshow pf-current-image (1+ pf-current-image) t))

(defun pf-set-time (n datetime)
  "Set the date and time of image N to DATETIME."
  (let ((image (elt pf-images n)))
    (let ((res (shell-command-to-string
		(concat "touch -d " datetime " "
			(pf-image->filename image)))))
      (unless (string= res "")
	(error "Error executing touch: %s" res)))
    (pf-image->forget-timestamp image))
  (pf-goto-image n))

(defun pf-set-current-time (datetime)
  "Set the time of an image"
  (interactive "sDate and time: ")
  (pf-set-time pf-current-image datetime))

(defun pf-goto-date (date)
  "Go to the first image of a the given date."
  (interactive "sGo to date: ")
  (let ((lo 0)
	(hi (length pf-images)))
    (while (< lo hi)
      (let* ((mid (/ (+ lo hi) 2))
	     (imdate (format-time-string
		      "%Y-%m-%d"
		      (pf-image->timestamp (elt pf-images mid)))))
	(if (string< imdate date)
	    (setq lo (1+ mid))
	  (setq hi mid))))
    (pf-goto-image lo)))

(defun pf-copy-tags-from-previous ()
  (interactive)
  (let ((img (elt pf-images pf-current-image))
	(prev (elt pf-images (1- pf-current-image))))
    (pf-image->set-tags
     img (copy-sequence (pf-image->tags prev)))
    (set-buffer-modified-p t)
    (pf-goto-image pf-current-image t)))

(defun pf-copy-tags ()
  (interactive)
  (setq pf-tag-paste-bin
	(copy-sequence (pf-image->tags (elt pf-images pf-current-image)))))

(defun pf-paste-tags ()
  (interactive)
  (pf-image->set-tags
   (elt pf-images pf-current-image)
   (copy-sequence pf-tag-paste-bin))
  (set-buffer-modified-p t)
  (pf-goto-image pf-current-image t))

(defun pf-diff-metadata-other-window ()
  (interactive)
  (delete-other-windows)
  (let* ((image (elt pf-images pf-current-image))
	 (metadata (pf-image->metadata image "props"))
	 (buf (get-buffer-create "*PF-metadiff*"))
	 (coding-system-for-read 'utf-8))
    (set-buffer buf)
    (erase-buffer)
    (call-process-shell-command
     (format "cd '%s' && git status '%s' && git diff '%s'"
	     (pf-image->metadata-root image)
	     metadata metadata)
     nil buf)
    (display-buffer buf)
    (other-window 1)
    (shrink-window-if-larger-than-buffer)
    (other-window -1)))

;;; Filtering

(defun pf-set-filter (filter)
  (interactive "xFilter: ")
  (setq pf-uncompiled-filter filter)
  (setq pf-compiled-filter (if (null filter)
				 nil
			     (pf-optimize-filter
			      (pf-compile-filter filter))))
  (pf-apply-filter))

(defun pf-filter-tag (tag)
  (interactive (list (pf-read-tag t)))
  (cond
   ((null pf-uncompiled-filter)
    (pf-set-filter tag))
   (t
    (pf-set-filter (list tag pf-uncompiled-filter)))))

(defun pf-filter-tag-regexp (tag)
  (interactive "sTag regexp: ")
  (let ((tags nil))
    (maphash (function (lambda (k v)
			 (if (string-match tag k)
			     (setq tags (cons k tags)))))
	     pf-tag-index)
    (message "Found %d matching tags: %s" (length tags) tags)
    (let ((new-filter (cons '| tags)))
      (message "new filter 1: %s" new-filter)
      (if pf-uncompiled-filter
	  (setq new-filter (list '& pf-uncompiled-filter new-filter)))
      (message "new filter 2: %s" new-filter)
      (pf-set-filter new-filter))))

(defun pf-filter-clear ()
  (interactive)
  (pf-set-filter nil))

(defun pf-apply-filter ()
  (interactive)
  (cond
   ((null pf-compiled-filter)
    (setq pf-active-list nil)
    (message "Filter removed"))
   (t
    (setq pf-active-list (sort (copy-sequence
				(pf-eval-filter pf-compiled-filter))
			       'pf-image-compare-n))
    (message "%d matching images." (length pf-active-list)))))

(defun pf-list-active-images ()
  (interactive)
  (let ((buf (generate-new-buffer "*Photofokus-images*")))
    (display-buffer buf)
    (set-buffer buf)
    (dolist (image pf-active-list)
      (pf-image->insert-description image))))

(defun pf-compile-filter (filter)
  (cond
   ((stringp filter)
    (pf-TAG filter))
   ((consp filter)
    (let ((op (car filter))
	  (data (cdr filter)))
      (cond
       ((or (string= op '&)
	    (string= op 'and))
	(pf-compile-filter-node data 'pf-AND))
       ((or (string= op '|)
	    (string= op 'or))
	(pf-compile-filter-node data 'pf-OR))
       ((or (string= op '!)
	    (string= op 'not))
	(pf-compile-filter-node data 'pf-NOT))
       (t
	(pf-compile-filter-node filter 'pf-AND)))))
   (t
    (error "Bad filter expression: %s" filter))))

(defun pf-compile-filter-node (filter ctor)
  (let ((node (apply ctor nil)))
    (dolist (element filter)
      (pf-NODE->add-node node (pf-compile-filter element)))
    node))

(defun pf-TAG (tag)
  (vector 'pf-TAG tag))

(defun pf-AND ()
  ; 0 opcode (pf-AND)
  ; 1 TAG
  ; 2 AND
  ; 3 OR
  ; 4 NOT
  ; 5 SUB
  (vector 'pf-AND nil nil nil nil nil))

(defun pf-OR ()
  ; 0 opcode (pf-OR)
  ; 1 TAG
  ; 2 AND
  ; 3 OR
  ; 4 NOT
  ; 5 SUB
  (vector 'pf-OR nil nil nil nil nil))

(defun pf-NOT ()
  ; 0 opcode (pf-NOT)
  ; 1 TAG
  ; 2 AND
  ; 3 OR
  ; 4 NOT
  ; 5 SUB
  (vector 'pf-NOT nil nil nil nil nil))

(defun pf-SUB ()
  ; 0 opcode (pf-SUB)
  ; 1 included
  ; 2 excluded
  (vector 'pf-SUB nil nil))

(defconst pf-NODE-index
  '((pf-TAG . 1)
    (pf-AND . 2)
    (pf-OR  . 3)
    (pf-NOT . 4)
    (pf-SUB . 5)))

(defun pf-NODE->op (node)
  (elt node 0))

(defun pf-NODE->add-node (node child)
  (unless (memq (pf-NODE->op node) '(pf-AND pf-OR pf-NOT))
    (error "Bad node type %s" (elt node 0)))
  (let* ((op (pf-NODE->op child))
	 (offset (cdr (assq op pf-NODE-index))))
    (aset node offset (cons child (elt node offset)))))

(defun pf-NODE->all-filters (node)
  (apply 'append (cdr (append node nil))))

(defun pf-optimize-filter (filter)
  (let ((pf-any-change t))
    (while pf-any-change
      (setq pf-any-change nil)
      (setq filter (pf-optimize-2 filter))))
  filter)

(defun pf-optimize-2 (filter)
  (let ((optimizers (cdr-safe (assq (pf-NODE->op filter) pf-optimizers)))
	(new-val filter))
    (while (and optimizers (eq new-val filter))
      (setq new-val (apply (car optimizers) filter nil))
      (setq optimizers (cdr optimizers)))
    (cond
     ((not (eq new-val filter))
      (setq pf-any-change t)
      new-val)
     ((eq (pf-NODE->op filter) 'pf-TAG)
      filter)
     (t
      (let ((categories (cdr (append filter nil)))
	    (new-filter (list (elt filter 0)))
	    change)
	(dolist (category categories)
	  (let (new-cat)
	    (dolist (item category)
	      (let ((new-item (pf-optimize-2 item)))
		(if (not (eq new-item item))
		    (setq change t))
		(setq new-cat (nconc new-cat (list new-item)))))
	    (setq new-filter (nconc new-filter (list new-cat)))))
	(if change
	    (vconcat new-filter)
	  filter))))))

(defun pf-optimize-and-not (filter)
  (if (and (consp (elt filter 4))
	   (or (consp (elt filter 1))
	       (consp (elt filter 3))))
      (let ((included (pf-AND))
	    (excluded (pf-AND))
	    (sub (pf-SUB)))
	(aset included 1 (elt filter 1))
	(aset included 2 (elt filter 2))
	(aset included 3 (elt filter 3))
	(aset included 5 (elt filter 5))

	(dolist (not-node  (elt filter 4))
	  (dolist (sub-node (pf-NODE->all-filters not-node))
	    (pf-NODE->add-node excluded sub-node)))

	(aset sub 1 (list included))
	(aset sub 2 (list excluded))
	sub)
    filter))

(defun pf-optimize-and-and (filter)
  (if (consp (elt filter 2))
      (let ((new-node (vconcat filter)))
	(aset new-node 2 nil)
	(dolist (and-node (elt filter 2))
	  (dolist (sub-node (pf-NODE->all-filters and-node))
	    (pf-NODE->add-node new-node sub-node)))
	new-node)
    filter))

(defun pf-optimize-or-or (filter)
  (if (consp (elt filter 3))
      (let ((new-node (vconcat filter)))
	(aset new-node 3 nil)
	(dolist (or-node (elt filter 3))
	  (dolist (sub-node (pf-NODE->all-filters or-node))
	    (pf-NODE->add-node new-node sub-node)))
	new-node)
    filter))

(defun pf-intersection (a b)
  "Return the intersection of a and b (both lists)."
  (let ((res nil))
    (while a
      (if (memq (car a) b)
	  (setq res (cons (car a) res)))
      (setq a (cdr a)))
    res))

(defun pf-union (a-list b-list)
  (let ((res nil))
    (dolist (a a-list)
      (unless (memq a res)
	(setq res (cons a res))))
    (dolist (b b-list)
      (unless (memq b res)
	(setq res (cons b res))))
    res))

(defun pf-subtract (a b)
  (let ((res nil))
    (while a
      (if (not (memq (car a) b))
	  (setq res (cons (car a) res)))
      (setq a (cdr a)))
    res))

(defconst pf-OP-assoc
  '((pf-TAG . pf-eval-tag)
    (pf-AND . pf-eval-and)
    (pf-OR  . pf-eval-or)
    (pf-NOT . pf-eval-not)
    (pf-SUB . pf-eval-sub)))

(defun pf-eval-filter (filter)
  (let* ((op (pf-NODE->op filter))
	 (fnc (cdr (assq op pf-OP-assoc))))
    (apply fnc filter nil)))

(defun pf-eval-tag (filter)
  (if (null pf-tag-index)
      (pf-find-all-tags))
  (gethash (elt filter 1) pf-tag-index))

(defun pf-eval-and (filter)
  (let ((subfilter (pf-NODE->all-filters filter)))
    (if (null subfilter)
	pf-images
      (let (parts)
	(dolist (part subfilter)
	  (setq parts (cons (pf-eval-filter part) parts)))
	(setq parts (sort parts (lambda (x y)
				  (< (length x) (length y)))))
	(let ((res (car parts)))
	  (dolist (images (cdr parts))
	    (setq res (pf-intersection res images)))
	  res)))))

(defun pf-eval-or (filter)
  (let (res)
    (dolist (part (pf-NODE->all-filters filter))
      (setq res (pf-union res (pf-eval-filter part))))
    res))

(defun pf-eval-not (filter)
  (pf-subtract pf-images (pf-eval-and filter)))

(defun pf-eval-sub (filter)
  (pf-subtract (pf-eval-and (car-safe (elt filter 1)))
	       (pf-eval-and (car-safe (elt filter 2)))))

(defun pf-export-per-day ()
  (interactive)
  (dolist (img pf-images)
    (let* ((timestamp (pf-image->timestamp img))
	   (year (format-time-string "%Y" timestamp))
	   (mon (format-time-string "%Y-%m" timestamp))
	   (date (format-time-string "%Y-%m-%d" timestamp))
	   (file (pf-image->rotated-filename img))
	   (excluded (equal "1" (car-safe
				 (pf-image->get-prop
				  img
				  "excl-slideshow"))))
	   (private  (equal "1" (car-safe
				 (pf-image->get-prop
				  img
				  "private")))))
      (when (not (or excluded private))
	(let ((dir (expand-file-name
		    date
		    (expand-file-name
		     mon
		     (expand-file-name
		      year
		      (expand-file-name "photos-by-date" "/thumbs"))))))
	  (make-directory dir t)
	  (make-symbolic-link file (expand-file-name
				    (format "%06d.jpg" (pf-image->n img))
				    dir)))))))

(defun pf-collate-insert (first-date last-date run-length tag)
  (if tag
      (if (string= first-date last-date)
	  (insert (format "%-22s %4d %s\n"
			  first-date
			  run-length tag))
	(insert (format "%10s--%10s %4d %s\n"
			first-date last-date
			run-length tag)))))

(defun pf-collate-framkalla ()
  (interactive)
  (let ((regexp "^framkalla\\($\\|t-\\)"))
    (pf-filter-clear)
    (pf-filter-tag-regexp regexp)

    (let ((buf (generate-new-buffer "*Photofokus-collate*"))
	  (current-tag nil)
	  (first-date nil)
	  (last-date nil)
	  (run-length 0))
      (display-buffer buf)
      (set-buffer buf)
      (dolist (image pf-active-list)
	(let ((image-tag nil))
	  (dolist (tag (pf-image->tags image))
	    (if (string-match regexp tag)
		(setq image-tag tag)))
	  (if (string= image-tag current-tag)
	      (setq run-length (1+ run-length))
	    (pf-collate-insert first-date last-date run-length current-tag)
	    (setq run-length 1)
	    (setq current-tag image-tag)
	    (setq first-date
		  (format-time-string "%Y-%m-%d"
				      (pf-image->timestamp image))))
	  (setq last-date (format-time-string
			   "%Y-%m-%d" (pf-image->timestamp image)))))
      (pf-collate-insert first-date last-date run-length current-tag))))
