#!/bin/bash
# 
# Create and populate a shadow directory with thumbnails of all images
# in a source directory.

PATH=/opt/netpbm/bin:$PATH
export PATH

if [ $# -ne 3 ] && [ $# -ne 4 ]
then
    echo usage: $0 maxsize class synctime [ image-filename ] >&2
    echo Use maxsize '"fullsize"' to use size from /tmp/natural-fullsize.txt >&2
    exit 1
fi

size=$1
class=$2
synctime=$3
if [ $size = fullscreen ]
then
    if [ -f /tmp/natural-fullsize.txt ]
    then
	read xsize ysize < /tmp/natural-fullsize.txt
	size=${xsize}x${ysize}
    else
	echo "/tmp/natural-fullsize.txt not found" >&2
	exit 1
    fi
else
    xsize=$size
    ysize=$size
fi
if [ $# -eq 4 ]
then
    filename=$4
else
    filename=""
fi

srcdir=/photos/$class/hotsync/$synctime
metadir=/photos/metadata/$class/$synctime

function update_image () {
    file="$1"

    if [ -f "$metadir/$file/rot" ]
    then
	rot=`cat "$metadir/$file/rot"`
    else
	rot=0
    fi

    updir="/thumbs/$class/0/$synctime/$size"
    dstdir="/thumbs/$class/$rot/$synctime/$size"
    mkdir -p "$updir" "$dstdir"

    upfile="$updir/$file"
    dstfile="$dstdir/$file"

    if ! [ -f "$upfile" ]
    then
	opts=""
	if [ $xsize -lt 196 ]
	then
	    opts="$opts -fast -scale 1/4"
	fi
	djpeg $opts "$srcdir/$file" \
	    | pnmscale -xysize $xsize $ysize \
	    | cjpeg > "$upfile".tmp && mv "$upfile".tmp "$upfile"
    fi
    if ! [ -f "$dstfile" ]
    then
	jpegtran -rotate `expr 360 - $rot` \
	    < "$upfile" \
	    > "$dstfile".tmp && mv "$dstfile".tmp "$dstfile"
    fi
}

function move_back()
{
    bs=$1
    while [ $bs -gt 0 ]
    do
	echo -e -n "\010"
	bs=`expr $bs - 1`
    done
}

old=$SECONDS
if [ "$filename" = "" ]
then
    tot=0
    for i in "$srcdir/"*.jpg "$srcdir/"*.JPG
    do
	if [ "$i" = "$srcdir/"'*.jpg' ] || [ "$i" = "$srcdir/"'*.JPG' ]
        then
    	    continue
        fi
	tot=`expr $tot + 1`
    done

    ctr=0
    bs=0
    for i in "$srcdir/"*.jpg "$srcdir/"*.JPG
    do
        if [ "$i" = "$srcdir/"'*.jpg' ] || [ "$i" = "$srcdir/"'*.JPG' ]
        then
    	    continue
        fi
	if (( ++ctr % 20 == 0 )) || [ "$SECONDS" != "$old" ]
	then
	    old=$SECONDS
	    move_back $bs
	    msg=" ($ctr/$tot)"
	    echo -n "$msg"
	    bs=${#msg}
	fi
        update_image "`basename \"$i\"`"
    done
    move_back $bs
    tput el
else
    update_image "$filename"
fi
