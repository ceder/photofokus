#!/usr/bin/env python3

import os
import sys
import subprocess

EXCLUDED_FILES = [
    b"contents.txt",
    b"complete",
    b"folder.txt",
    b"sha256sum.txt",
]

EXCLUDED_DIRS = [
    b"sums",
    b"metadata",
]

def remove_from_list(lst, exlusions):
    for e in exlusions:
        if e in lst:
            lst.remove(e)

def usage():
    sys.stderr.write("bad usage\n")
    sys.exit(1)

def scan_files(root, files, sum):
    files.sort()
    res = subprocess.run(["/usr/bin/sha256sum"] + files,
                         cwd=root,
                         check=True,
                         capture_output=True)
    os.makedirs(os.path.dirname(sum), exist_ok=True)
    with open(sum, "wb") as fp:
        fp.write(res.stdout)
    print(sum)

def rescan_dir(root, files, sums, sum):
    for line in sums.split(b"\n"):
        if line == b"":
            continue
        sha256, filename = line.split(b"  ", 2)
        if filename in files:
            files.remove(filename)
        else:
            print(b"File missing: " + filename)
    for f in files:
        print(b"Unexpected file: " + root + b"/" + f)

def removeprefix(s, p):
    if s.startswith(p):
        return s[len(p):]
    else:
        return s

def scan_dir(root, files):
    remove_from_list(files, EXCLUDED_FILES)
    d = removeprefix(root, b"/photos/")
    sum = b"/photos/sums/" + d + b"/sha256sum.txt"
    if os.path.exists(sum):
        with open(sum, "rb") as fp:
            sums = fp.read()
        rescan_dir(root, files, sums, sum)
    else:
        scan_files(root, files, sum)

def scan():
    scanned = []
    for root, dirs, files in os.walk(b"/photos"):
        remove_from_list(dirs, EXCLUDED_DIRS)
        if b"complete" in files:
            scan_dir(root, files)
            scanned.append(removeprefix(root, b"/photos/"))

    for root, dirs, files in os.walk(b"/photos/sums"):
        remove_from_list(dirs, [b".git"])
        if b"sha256sum.txt" in files:
            existing = removeprefix(root, b"/photos/sums/")
            if existing not in scanned:
                print(b"Missing dir: " + existing)

def main():
    if len(sys.argv) < 2:
        usage()

    if sys.argv[1] == "scan":
        scan()
    else:
        usage()

    sys.exit(0)

if __name__ == '__main__':
    main()
