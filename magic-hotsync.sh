#!/bin/bash
SERIAL=HT95NKF08367
USB_ID="usb-HTC_Android_Phone_$SERIAL-0:0-part1"
CLASS=magic

BASE=/photos/$CLASS
NOW=`date +%Y-%m-%dT%H:%M:%S`
SYNCDIR=$BASE/hotsync/$NOW
VIEWDIR=$BASE/hotview/$NOW
THUMBDIR=/thumbs/$CLASS/0/$NOW
umask 022

CAMERADIRS="/mnt/DCIM/100MEDIA /mnt/CameraMagic /mnt/dcim/Camera /mnt/DCIM/VIDEO"

mount "/dev/disk/by-id/$USB_ID" /mnt || exit 1
for cameradir in $CAMERADIRS
do
    if [ `ls $cameradir | wc -l` = 0 ]
    then
	continue
    fi

    mkdir $SYNCDIR || exit 1
    mkdir $VIEWDIR || exit 1
    cd $SYNCDIR || exit 1

    mv $cameradir/* .

# for f in *.jpg
# do
#   d=$(jhead $f \
#       |sed -n 's/Date.Time *: '//p\
#       |sed -e s/:/-/ -e s/:/-/ )
#   if [ -n "$d" ]
#   then
#       touch -d "$d" $f 
#   fi
# done
    touch complete
    chmod 444 *

    /home/ceder/hg/work/photofokus/work/imagemap.sh $CLASS $NOW

    chown -R ceder:ceder $VIEWDIR $THUMBDIR
    chmod 644 $SYNCDIR/*

    echo file://$VIEWDIR/part-1.html
    umount /mnt
    exit 0
done
umount /mnt
echo No images found
exit 0
