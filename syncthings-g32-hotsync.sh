#!/bin/bash
CLASS=moto-g32
BASE=/photos/$CLASS
NOW=`date +%Y-%m-%dT%H:%M:%S`
SYNCDIR=$BASE/hotsync/$NOW
VIEWDIR=$BASE/hotview/$NOW
THUMBDIR=/thumbs/$CLASS/0/$NOW
cameradir="/home/ceder/syncthing/Moto-G32-Camera/Camera"
umask 022

if [ `ls "$cameradir" | wc -l` = 0 ]
then
    echo No new photos found
    exit 0
fi

mkdir $SYNCDIR || exit 1
mkdir $VIEWDIR || exit 1
cd $SYNCDIR || exit 1
mv "$cameradir"/* .
touch complete
chmod 444 *

/home/ceder/rgit/photofokus/imagemap.sh $CLASS $NOW

chown -R ceder:ceder $VIEWDIR $THUMBDIR
chmod 644 $SYNCDIR/*
echo file://$VIEWDIR/part-1.html
exit 0
