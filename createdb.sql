drop database if exists photofokus;
create database photofokus;
use photofokus;

create table base (
	dir_id int32 auto_increment	primary key not null,
	dir_name varchar(255)		not null,
	active bool			not null,

	UNIQUE(dir_name),
);

create table photo (
	photo_id int32 auto_increment		primary key not null,
	filename varchar(255)			not null,
	dir_id int64				not null
		references base(dir_id),
	photo_taken datetime			not null,
	size int32				not null,
	res_x int16				not null,
	res_y int16				not null,
	width int16				not null,
	height bool				not null,
	focal float				null,
	exposure float				null,

	INDEX(basename),
	INDEX(photo_taken)
);

create table variant (
	variant_id int32 auto_increment		primary key not null,
	photo_id int32				not null
		references photo(photo_id),
	
);

insert into base (dir_name, active) values ("/photos/olymp", 1);
insert into base (dir_name, active) values ("/photos/dc", 1);
