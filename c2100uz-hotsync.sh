#!/bin/sh
exec > /tmp/c2100uz-hotsync.log 2>&1
date
echo Hotsync started
/bin/mkdir -p /var/run/usb
/bin/rm /var/run/usb/c2100uz-removalkey || true
set -x
CLASS=olymp
BASE=/photos/$CLASS
NOW=`date +%Y-%m-%dT%H:%M:%S`
SYNCDIR=$BASE/hotsync/$NOW
VIEWDIR=$BASE/hotview/$NOW
THUMBDIR=/thumbs/$CLASS/0/$NOW
mkdir $SYNCDIR || exit 1
mkdir $VIEWDIR || exit 1
cd $SYNCDIR || exit 1
gphoto2 --port usb: --camera "Olympus C-2100UZ" --get-all-files || exit 1

echo '#!/bin/sh' > $REMOVER
echo '/bin/rm /var/run/usb/c2100uz-removalkey' >>$REMOVER
chmod +x $REMOVER

dd if=/dev/random bs=16 count=1 \
| od -x \
| head -1 \
| sed -e 's/0* //' -e 's/ //g' > $VIEWDIR/removalkey
cp $VIEWDIR/removalkey /var/run/usb/c2100uz-removalkey

for f in *.JPG
do
  d=$(jhead $f \
      |sed -n 's/Date.Time *: '//p\
      |sed -e s/:/-/ -e s/:/-/ )
  if [ -n "$d" ]
  then
      touch -d "$d" $f 
  fi
done
touch complete

/usr/local/sbin/c2100uz-imagemap $CLASS $NOW

chown -R ceder:ceder $VIEWDIR $THUMBDIR
chmod 644 $SYNCDIR/*

LOGNAME=ceder
DISPLAY=:1.0
export LOGNAME
export DISPLAY
su ceder -c "mozilla -P default -display :1 -remote \"OpenURL(file://$VIEWDIR/part-1.html,new-tab)\""
