#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

static void
check_creds(void)
{
  char *len = getenv("CONTENT_LENGTH");
  char cgi_input[38];
  int fd;
  char buf[32];

  if (len == NULL)
    {
      fprintf(stderr, "Missing CONTENT_LENGTH\n");
      exit(1);
    }

  if (strcmp(len, "38"))
    {
      fprintf(stderr, "Broken CONTENT_LENGTH %s\n", len);
      exit(1);
    }

  if (read(0, cgi_input, sizeof(cgi_input)) != sizeof(cgi_input))
    {
      perror("read");
      exit(1);
    }

  if (strncmp(cgi_input, "creds=", 6))
    {
      fprintf(stderr, "Broken form\n");
      exit(1);
    }

  fd = open("/var/run/usb/c2100uz-removalkey", O_RDONLY);
  if (fd < 0)
    {
      if (errno == ENOENT)
	{
	  printf("The camera isn't connected, or the key is removed.\n");
	  exit(1);
	}
    }
  if (read(fd, buf, sizeof(buf)) != sizeof(buf))
    {
      perror("reading removalkey");
      exit(1);
    }
  close(fd);
  if (memcmp(cgi_input + 6, buf, sizeof(buf)))
    {
      printf("Bad key credentials.\n");
      remove("/var/run/usb/c2100uz-removalkey");
      exit(1);
    }
}

int
main(void)
{
  printf("Content-type: text/plain\r\n\r\n");
  fflush(stdout);

  if (setuid(0) < 0)
    {
      perror("setuid");
      return 1;
    }
  
  if (setgid(0) < 0)
    {
      perror("setgid");
      return 1;
    }

  check_creds();
  if (system("/usr/bin/gphoto2 --port usb: --camera \"Olympus C-2100UZ\" "
	     "--delete-all-files") != 0)
    {
      perror("system");
      return 1;
    }
  remove("/var/run/usb/c2100uz-removalkey");
  printf("Ready.\n");
  return 0;
}
