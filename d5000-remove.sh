#!/bin/bash
CLASS=d5000
BASE=/photos/$CLASS
gphoto2 --list-files > /tmp/contents.txt
for i in $BASE/hotsync/*/contents.txt
do
    if cmp $i /tmp/contents.txt >/dev/null
    then 
	echo match found:$i
	FOLDER=`echo $i|sed s/contents.txt/folder.txt/`
    fi
done
echo folder:
cat $FOLDER
gphoto2 --port usb: --camera "USB PTP Class Camera" --delete-all-files --folder `cat $FOLDER` || exit 1

exit 0
