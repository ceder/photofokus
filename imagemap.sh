#!/bin/bash

if [ $# -ne 2 ]
then
    echo usage: $0 class now >&2
    exit 1
fi

CLASS=$1
NOW=$2
SYNCDIR=/photos/$CLASS/hotsync/$NOW
VIEWDIR=/photos/$CLASS/hotview/$NOW
THUMBDIR=/thumbs/$CLASS/0/$NOW

imgs=49
cols=7
size=64
xmargin=20
ymargin=20

/usr/local/bin/updatethumbnails -s $size -c $CLASS -t $NOW

TS=$THUMBDIR/$size

header () {
    exec 10>$VIEWDIR/part-$file.html
    echo "<html><head><title>Imagemap $file</title></head><body bgcolor=black text=yellow link=lightblue vlink=blue alink=blue><table><tr>" >&10
    echo "<a href=\"part-$file.html\">Part $file</a>" >> $VIEWDIR/index.html
}

footer () {
    echo '</tr></table><br>' >&10
    if [ $file -gt 1 ]
    then
	echo "<a href=\"part-$(($file - 1)).html\">Prev</a>" >&10
    else
	echo Prev >&10
    fi
    if [ $# -gt 0 ]
    then
	echo "<a href=\"part-$[$file + 1].html\">Next</a>" >&10
    else
	echo Next >&10
    fi
    echo "<a href=\"index.html\">Top</a>" >&10
    echo This is part $file >&10
    if [ $# = 0 ] && [ -f $VIEWDIR/removalkey ]
    then
        echo '<form method=post' >&10
	echo '   action="http://localhost/cgi-bin/removephotos">' >&10
	echo "  <input type=hidden name=creds value=\"`cat $VIEWDIR/removalkey`\">" >&10
	echo '  <input type=submit value="Remove from camera">' >&10
	echo '</form>' >&10
    fi
    echo "</body></html>" >&10
    exec 10>&-
}

nextfile () {
    if [ -n "$file" ]
    then
	footer more
	file=$((file + 1))
    else
	file=1
    fi
    header
}

cat << \EOF > $VIEWDIR/index.html
<html><head><title>Imagemap</title></head>
<body><h1>Imagemap</h1>
EOF

n=0
tot=1
for i in "$TS"/*.jpg "$TS"/*.JPG
do
    if [ "$i" = "$TS"/'*.jpg' ] || [ "$i" = "$TS"/'*.JPG' ]
    then
	continue
    fi

    n=$[$n + 1]
    if [ $n -gt $imgs ]
    then
	n=1
	tot=$[tot + 1]
    fi
done

n=0
nextfile
    
for i in "$TS"/*.jpg "$TS"/*.JPG
do
    if [ "$i" = "$TS"/'*.jpg' ] || [ "$i" = "$TS"/'*.JPG' ]
    then
	continue
    fi

    n=$(($n + 1))
    if [ $n -gt $imgs ]
    then
	echo ' '$file/$tot
	nextfile
	n=1
    fi
    if [ `expr $n % $cols` = 1 ] && [ $n -gt $cols ]
    then
	echo -e '</tr>\n<tr>' >&10
    fi
    base=$(basename $i)
    echo -n '<td align=center valign=center width='$[$size + $xmargin]' height='$[$size + $ymargin]'><a href="'"$SYNCDIR/$base"'"><img src="'"$i"'"><br>' >&10
    echo -n $base >&10
    echo '</a></td>' >&10
    echo -n .
done
footer

cat << \EOF >> $VIEWDIR/index.html
</body></html>
EOF
echo
