CC=gcc
CFLAGS=-O2 -Wall

removephotos: removephotos.o

CGI=/var/www/localhost/cgi-bin

install:
	cp updatethumbnails.py /usr/local/bin/updatethumbnails

install-c2100: removephotos
	cp c2100uz-hotsync.sh /etc/hotplug/usb/c2100uz-hotsync
	cp imagemap.sh /usr/local/sbin/c2100uz-imagemap
	cp olympus-c2100uz.usermap /etc/hotplug/usb/olympus-c2100uz.usermap
	cp removephotos $(CGI)
	chown 0:0 $(CGI)/removephotos
	chmod 4755 $(CGI)/removephotos
