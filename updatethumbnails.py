#!/usr/bin/env python3

import re
import os
import sys
import time
import errno
import getopt
import subprocess

CLASSES = ["other", "d5000", "nexus", "s520", "magic", "olymp", "dc",
           "kristina", "moto-g7", "moto-g32",
           "kristina-moto-g5",
           "kristina-moto-g30",
           "kristina-moto-g32",
           "kristina-moto-g34"]
SIZES = [64, 128, 512] # Not worth the space: , "fullscreen"]

TIME_RE = re.compile("^[1-9][0-9]{3}-[01][0-9]-[0-3][0-9]"
                     "T[0-2][0-9]:[0-6][0-9]:[0-6][0-9]$")

def metadir(camera, synctime):
    return os.path.join("/photos/metadata", camera, synctime)

def read_file(fn):
    fp = open(fn)
    res = fp.read()
    fp.close()
    return res

def rotation(camera, synctime, image):
    try:
        return int(read_file(os.path.join(metadir(camera, synctime),
                                          image, "rot")))
    except IOError as err:
        if err.errno != errno.ENOENT:
            raise
        return 0

class JobQueue(object):
    def __init__(self, output):
        self.__pending = {}
        self.__syncs = []
        self.__images = 0
        self.__output = output

    def add_image(self, camera, synctime, srcfile):
        key = camera + "/" + synctime
        if key not in self.__pending:
            self.__syncs.append((camera, synctime))
            self.__pending[key] = []
        self.__pending[key].append(srcfile)
        self.__images += 1

    def mark_end_of_file(self):
        processed = 0
        last = time.time()
        for (nr, (camera, synctime)) in enumerate(self.__syncs):
            self.__output.enter_dir(camera, synctime)
            key = camera + "/" + synctime
            sys.stderr.write("Processing %s (%d/%d)" % (
                    key, nr+1, len(self.__pending)))
            sys.stderr.flush()
            msg = ""
            for img_nr, image in enumerate(self.__pending[key]):
                if time.time() != last or img_nr < 10 or img_nr % 100 == 0:
                    last = time
                    sys.stderr.write("\010" * len(msg))
                    msg = " (%d/%d) (%d/%d)" % (
                        img_nr+1, len(self.__pending[key]),
                        processed+1, self.__images)
                    sys.stderr.write(msg)
                    sys.stderr.flush()
                self.__output.perform_update(image)
                processed += 1
            if msg != "":
                sys.stderr.write("\010" * len(msg))
                sys.stderr.flush()
                subprocess.call(["tput", "el"])
            sys.stderr.write("\n")
            self.__output.dir_updated()


class Size(object):
    def __init__(self, size):
        self.xsize = size
        self.ysize = size
        self.name = str(size)

    def enter_dir(self, camera, synctime):
        self.camera = camera
        self.synctime = synctime
        self.marker = os.path.join("/thumbs", camera, "0",
                                   synctime, self.name, "sha1")

    def dir_need_update(self, current_commit):
        try:
            fp = open(self.marker)
            generating_commit = fp.read().strip()
            fp.close()
        except IOError as err:
            if err.errno != errno.ENOENT:
                raise
            return True

        if generating_commit == current_commit:
            return False

        rc = subprocess.call(["git", "diff", "--quiet",
                              generating_commit, current_commit,
                              "--", self.camera + "/" + self.synctime],
                             cwd="/photos/metadata")
        if rc == 1:
            return True

        self.dir_updated(current_commit)
        return False

    def dir_updated(self, current_commit):
        fp = open(self.marker, "w")
        fp.write(current_commit + "\n")
        fp.close()

    def enter_image(self, image):
        self.image = image
        self.rotation = rotation(self.camera, self.synctime, self.image)
        self.updir = os.path.join("/thumbs", self.camera, "0",
                                  self.synctime, self.name)
        self.dstdir = os.path.join("/thumbs", self.camera, str(self.rotation),
                                   self.synctime, self.name)
        self.upfile = os.path.join(self.updir, self.image)
        self.dstfile = os.path.join(self.dstdir, self.image)

    def image_need_update(self):
        if os.path.exists(self.dstfile) and os.path.exists(self.upfile):
            return False
        else:
            return True

    def start_conversion(self):

        try:
            os.makedirs(self.updir)
        except OSError as err:
            if err.errno != errno.EEXIST:
                raise

        try:
            os.makedirs(self.dstdir)
        except OSError as err:
            if err.errno != errno.EEXIST:
                raise

        if os.path.exists(self.upfile):
            self.scaler = None
            self.compressor = None
            return

        (pipe_r, pipe_w) = os.pipe()

        self.scaler = subprocess.Popen(
            ["pnmscale", "-xysize", str(self.xsize), str(self.ysize)],
            stdin=subprocess.PIPE,
            stdout=pipe_w,
            close_fds=True)

        self.compressor = subprocess.Popen(
            ["cjpeg"],
            stdin=pipe_r,
            stdout=open(self.upfile + ".tmp", "w"),
            close_fds=True)

        os.close(pipe_r)
        os.close(pipe_w)

    def feed(self, block):
        if self.scaler is not None:
            self.scaler.stdin.write(block)

    def finalize_conversion(self):
        if self.scaler is not None:
            self.scaler.stdin.close()
            scaler_ret = self.scaler.wait()
            compressor_ret = self.compressor.wait()
            if scaler_ret != 0:
                self.stderr.write("pnmscale failed\n")
            if compressor_ret != 0:
                self.stderr.write("djpeg failed\n")
            if scaler_ret != 0 or compressor_ret != 0:
                os.remove(self.upfile + ".tmp")
                return # FIXME: Error handling
            os.rename(self.upfile + ".tmp", self.upfile)
        if not os.path.exists(self.dstfile):
            subprocess.check_call(
                ["jpegtran", "-rotate", str(360 - self.rotation)],
                stdin=open(self.upfile),
                stdout=open(self.dstfile + ".tmp", "w"))
            os.rename(self.dstfile + ".tmp", self.dstfile)

class FullScreen(Size):
    def __init__(self):
        if os.path.exists("/tmp/natural-fullsize.txt"):
            fp = open("/tmp/natural-fullsize.txt")
            parts = fp.readline().split()
            fp.close()
            self.xsize = int(parts[0])
            self.ysize = int(parts[1])
            self.name = "%dx%d" % (self.xsize, self.ysize)
        else:
            self.name = None

class Output(object):
    def __init__(self, sizes, current_commit):
        self.__sizes = []
        self.__current_commit = current_commit
        for s in sizes:
            if s == "fullscreen":
                tmp = FullScreen()
            else:
                tmp = Size(s)
            if tmp.name is not None:
                self.__sizes.append(tmp)

    def dir_need_update(self, camera, synctime):
        if self.__current_commit is None:
            return True
        for s in self.__sizes:
            s.enter_dir(camera, synctime)
            if s.dir_need_update(self.__current_commit):
                return True
        return False

    def enter_dir(self, camera, synctime):
        self.__camera = camera
        self.__synctime = synctime
        self.__extract_thumbnail = True
        for s in self.__sizes:
            s.enter_dir(camera, synctime)
            if s.xsize > 120 or s.ysize > 120:
                self.__extract_thumbnail = False

    def perform_update(self, image):
        dst = []
        for s in self.__sizes:
            s.enter_image(image)
            if s.image_need_update():
                dst.append(s)
        if len(dst) == 0:
            return
        for s in dst:
            s.start_conversion()
        fn = os.path.join("/photos", self.__camera, "hotsync",
                          self.__synctime, image)
        thumbnail = None
        if self.__extract_thumbnail:
            jhead = subprocess.Popen(["jhead", "-st", "-", fn],
                                     stdout=subprocess.PIPE,
                                     stderr=subprocess.PIPE)
            thumbnail = jhead.stdout.read()
            err = jhead.stderr.read()
            if jhead.wait() != 0:
                self.__extract_thumbnail = False
                thumbnail = None
        if thumbnail is None or len(thumbnail) == 0:
            djpeg = subprocess.Popen(["djpeg", fn], stdout=subprocess.PIPE)
        else:
            djpeg = subprocess.Popen(["djpeg"], stdout=subprocess.PIPE,
                                     stdin=subprocess.PIPE)
            djpeg.stdin.write(thumbnail)
            djpeg.stdin.close()

        while True:
            block = djpeg.stdout.read(10 * 1024 * 1024)
            if len(block) == 0:
                break
            for s in dst:
                s.feed(block)
        if djpeg.wait() != 0:
            sys.stderr.write("djpeg failed\n")
            return # FIXME: Error handling
        for s in dst:
            s.finalize_conversion()

    def dir_updated(self):
        if self.__current_commit is None:
            return
        for s in self.__sizes:
            s.dir_updated(self.__current_commit)

def process(classes, sizes, synctimes, ignore_commit_id):
    if ignore_commit_id:
        current_commit = None
    else:
        current_commit = os.popen("cd /photos/metadata "
                                  "&& git rev-parse HEAD").read().strip()
    output = Output(sizes, current_commit)
    jobqueue = JobQueue(output)
    for camera in classes:
        hotsyncdir = os.path.join("/photos", camera, "hotsync")
        for synctime in sorted(os.listdir(hotsyncdir), reverse=True):
            if not TIME_RE.match(synctime):
                continue
            if synctimes is not None and synctime not in synctimes:
                continue
            srcdir = os.path.join(hotsyncdir, synctime)
            if not os.path.exists(os.path.join(srcdir, "complete")):
                continue

            if not output.dir_need_update(camera, synctime):
                continue

            for srcfile in sorted(os.listdir(srcdir)):
                if not (srcfile.endswith(".jpg") or srcfile.endswith(".JPG")):
                    continue
                jobqueue.add_image(camera, synctime, srcfile)
    jobqueue.mark_end_of_file()

def main():
    cameras = []
    sizes = []
    synctimes = []
    ignore_commit_id = False
    opts, args = getopt.getopt(sys.argv[1:], "c:s:t:i")
    for opt, val in opts:
        if opt == '-c':
            cameras.append(val)
        elif opt == '-s':
            if val != "fullscreen":
                val = int(val)
            sizes.append(val)
        elif opt == '-t':
            synctimes.append(val)
        elif opt == '-i':
            ignore_commit_id = True
        else:
            sys.stderr.write("getopt error\n")
            sys.exit(1)

    if len(args) > 0:
        cameras += args

    if len(cameras) == 0:
        cameras = CLASSES
    if len(sizes) == 0:
        sizes = SIZES
    if len(synctimes) == 0:
        synctimes = None
    else:
        synctimes = set(synctimes)

    process(cameras, sizes, synctimes, ignore_commit_id)

if __name__ == '__main__':
    main()
